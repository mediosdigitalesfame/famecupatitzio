<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Finanaciamiento</title>
	<?php include('contenido/head.php'); ?>
</head>
<body>

	<?php include('chat.php'); ?>

	<!-- Container -->
	<div id="container">
    	<?php include('contenido/header.php'); ?>
		<?php include('contenido/analytics.php'); ?>
		<div id="content">

			<div class="page-banner">         

				<div class="container">
					<h2><strong>Financiamiento</strong></h2>
				</div>
			</div>

			<div class="about-box">
				<div class="container">
					<div class="row">
				</div>
			</div>



<!--barra información limpia-->
			<!-- blog-box Banner -->
			<div class="single-project-page">
				<div class="container">
					<div class="row">

						
 <div class="welcome-box">
					<div class="container">
						<h1><span>Financiamiento Chevrolet</span></h1>
						<br>
						<p align="justify"><strong>GM Financial es una de las principales financieras a nivel global.</strong><br><br>

Opera en 40 países y ha financiado más de 146 millones de vehículos. GM Financial se ha vuelto la compañía líder en el segmento de automóviles y ofrece al público las mejores opciones de financiamiento para comprar vehículos de la empresa automotriz más grande del mundo, General Motors.</p><br>
                 
<p align="justify">
En conjunto, GM Financial y General Motors ofrecen la variedad más amplia de opciones, los precios más competitivos y los planes más flexibles. La asesoría profesional brindada por GM Financial y las distribuidoras de General Motors te ayudarán a elegir el plan que mejor se ajuste a tus necesidades de forma sencilla y rápida.</p><br><br><br><br>

 

<br><br><br><br><br><br><br><br>

<h1> Cotiza tu Chevrolet </h1><br><br>
        
	<div class="about-box">
				 <div class="container">
				 	 <div class="col-md-3" align="center">
					 </div>     	 
                     <div class="col-md-6" align="center">
							 <?php include('form.php'); ?>
					 </div>     	 
			     </div>
			 </div>

					</div>
				</div>
			</div>

		</div><br><br><br><br><br>

		</div> 

		<br><br><br><br><br><br><br>

			<?php include('contenido/footer.php'); ?>
     </div> 			
	
</body>
</html>