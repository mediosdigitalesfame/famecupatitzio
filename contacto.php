<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Ponte en Contacto</title>
	<?php include('contenido/head.php'); ?>
</head>
<body>

	<?php include('chat.php'); ?>

	<!-- Container -->
	<div id="container">
		<?php include('contenido/header.php'); ?>
		<?php include('contenido/analytics.php'); ?>
		<div id="content">

			<div class="page-banner">
				<div class="container">
					<h2>Contáctanos</h2>

				</div>
			</div>


			<!-- contact box -->
			<div class="contact-box">
				<div class="container">
					<div class="row">

						<div class="col-md-6" align="center">

							<div class="container">
								<div class="col-md-12" >
									<a href="https://api.whatsapp.com/send?phone=524431810975&text=Hola,%20Quiero%20más%20información!" target="_blank" title="WhatsApp">
										<button type="button" class="btn btn-success"><i class="fa fa-whatsapp fa-3x">
										</i> <font size="6"> WhatsApp</font> 
									</button>
								</a>
							</div>
						</div>

						<br>

						<div class="container">
							<div class="col-md-12" >
								<?php include('form.php'); ?>
							</div>
						</div>
					</div>

					<div class="col-md-3">
						<div class="contact-information">
							<h3>Información de Contacto</h3>
							<ul class="contact-information-list">
								<li><span><i class="fa fa-home"></i>Av. Prolongación Lázaro Cárdenas #3591</span> <span>Fracc. Jardines del Bosque</span><span>C.P. 60190</span> <span>Uruapan, Michoacán</span></li>
								<li><span><i class="fa fa-phone"></i><strong>(452) 503 3900</strong></span></li>
								<li><i class="fa fa-phone"></i><span>Taller de Servicio <strong>Ext. 127</strong></span><br>
									<i class="fa fa-phone"></i><span>Postventa <strong>Ext. 112</strong></span><br>
									<i class="fa fa-phone"></i><span>Refacciones <strong>Ext. 112</strong></span><br>
									<i class="fa fa-phone"></i><span>Ventas <strong>Ext. 103</strong></span><br>
									<i class="fa fa-phone"></i><span>Seminuevos <strong>Ext. 109</strong></span><br>
									<i class="fa fa-phone"></i><span>Recepción <strong>Ext. 101</strong></span><br>  

									<i class="fa fa-phone"></i><span>Financiamiento <strong>Ext. 117</strong></span><br>                             
								</li>
								<p>
									<h3>Whatsapp</h3>
									<li>
										<span><i class="fa fa-whatsapp"></i>
											<strong>  
												Ventas y Postventa <br>
												<a href="https://api.whatsapp.com/send?phone=524431810975&text=Hola,%20Quiero%20más%20información!" title="Ventas">4431810975</a> | 
												<a href="https://api.whatsapp.com/send?phone=524521000646&text=Hola,%20Quiero%20más%20información!" title="Post Venta">4521000646</a>
											</strong>
										</span>
									</li>

									<h3>Whatsapp</h3>
									<li>
										<span><i class="fa fa-whatsapp"></i><strong>  
											Citas de Servicio <br>
											<a href="https://api.whatsapp.com/send?phone=524432732794&text=Hola,%20Quiero%20más%20información!" title="Cita de Servicio">4432732794</a> | 
											<a href="https://api.whatsapp.com/send?phone=524433771717&text=Hola,%20Quiero%20más%20información!" title="Cita de Servicio">4433771717</a>
										</strong>
									</span></li>

									<strong></strong>
								</ul>
							</div>
						</div>

						<div class="col-md-3">
							<div class="contact-information">
								<h3>Horario de Atención</h3>
								<p>Con gusto esperamos tu llamada en nuestro <strong>Call Center</strong>, para cualquier duda, aclaración o sugerencia que quieras compartirnos en <strong>Chevrolet FAME</strong>; te escuchamos y atendemos de manera personalizada. </p>
								<p class="work-time"><span>Lunes - Viernes</span> : 9:00 a.m. - 7:00 p.m.</p>
								<p class="work-time"><span>Sábado</span> : 9:00 a.m. - 2:00 p.m.</p>

							</div>
						</div>

					</div>
				</div>
			</div>

		</div> 

		<br>
		<br>

		<?php include('contenido/footer.php'); ?>
	</div> 			
	
</body>
</html>