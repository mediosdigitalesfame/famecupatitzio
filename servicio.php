<!doctype html>
<html lang="es" xml:lang="es" class="no-js">
<head>
	<title>Cita de Servicio</title>
	<?php include('contenido/head.php'); ?>
</head>
<body><em></em>

	<?php include('chat.php'); ?>

	<!-- Container -->
	<div id="container">
    	<?php include('contenido/header.php'); ?>
		<?php include('contenido/analytics.php'); ?>
		<div id="content">

			<div class="page-banner">
				<div class="container">
					<h2>Servicio Chevrolet FAME</h2>

				</div>
			</div>


			<!-- contact box -->
			<div class="contact-box">
				<div class="container">

				<center><h3>Cita de Servicio</h3></center>
  					<div class="col-md-3" align="center">
					</div>     	 
                    <div class="col-md-6" align="center">
							<?php include('form.php'); ?>
					</div>
				</div>
				<div class="section">
			<div id="about-section">
<br><br>

              <div class="welcome-box">
					<div class="container">
						<h1><span>Valor es el mejor precio todo el año</span></h1><br>
						<p align="justify">En Chevrolet Servicio® la relación con nuestros clientes no termina con la venta, sino comienza con ella. Por eso, para brindar un servicio de gran calidad, contamos con la más completa red de concesionarios distribuidos por todo el país, con el trato personal y la más alta tecnología que sólo se puede esperar de una marca líder, como es Chevrolet®.
Al venir con nuestros técnicos, usted podrá sentirse seguro de su desempeño, ya que están constantemente actualizándose por medio de un programa de capacitación. Programa que también incluye a nuestro consultor de servicio y nuestra mano de obra calificada, con la cual usted estará experimentando un servicio de expertos, ya que se encuentra en constante preparación para resolver cualquier inconveniente posible, sumado a:</p><br>
                 
<p align="justify">
- Una atención personalizada<br>
- Refacciones originales<br>
- Herramientas especializadas<br>
- Centros de servicio con la tecnología adecuada<br>
- Diagnósticos específicos para tu Chevrolet®</p><br><br>

<!-- <p align="left">Te invitamos a conocer los programas y promociones que hemos diseñado especialmente para ti.</p><br>                        

<br><br>


<br>

						 <h1><span>Precios de Mantenimiento</span></h1><br>
							<div class="single-project-content">
                            	
                            	<div class="col-md-6">
								<img alt="" src="images/servicio-basico1.jpg">
                                </div>
                               
                                <div class="col-md-6">
								<img alt="" src="images/servicio-basico2.jpg">
                                </div>
                               	<br></div></div>
                                
                              <div class="container"><p align="justify"> 
                               Precios incluyen aceite de motor SINTÉTICO DEXOS, refacciones (original o ACDelco según aplique) y mano de obra de acuerdo al programa de mantenimiento de la póliza del vehículo. Todos los precios incluyen IVA. Válidos en la República Mexicana del 01 de junio al 31 de diciembre de 2016. Precios sugeridos por General Motors de México S. de R.L. de C.V. y sujetos a cambio sin previo aviso. Para información de otros vehículos, aplicabilidad o de las garantías correspondientes a los productos y servicios pregunte a su Distribuidor Autorizado Chevrolet.
                         	</p> </div>
                   <div class="services-box">
					<div class="container">
						<div class="row">
                            
							                           

							
                            
							
						</div>
					</div>
					<img class="shadow-image" alt="" src="images/shadow.png">
				</div> -->

</div>


</div></div></div>

<?php include('contenido/footer.php'); ?>

</body>
</html>